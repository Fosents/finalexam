import Pages.AccountLoginPage;
import Pages.RegistrationPage;
import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.Browser;
import utils.PageVerification;

public class RegistrationFormTest {

    @BeforeMethod
    public void setUp() {
        Browser.open();
    }

    @Test
    public void testRegForm() {
        WebDriverWait wait = new WebDriverWait(Browser.driver, 10);
        RegistrationPage.goTo();

        PageVerification.verifyPage("Register Account", "Not the page you expect!");

        RegistrationPage.enterData("Penio", "Todorov", "todorova" + RandomString.make(4) + "@gmail.com",
                "0897564736", "proba123VV", "proba123VV");
        RegistrationPage.clickSubscribe();
        RegistrationPage.clickAgree();
        RegistrationPage.clickContinue();
        wait.until(ExpectedConditions.titleContains("Your Account Has Been Created!"));
        PageVerification.verifyPage("Your Account Has Been Created!", "The page is not as expected");
    }

    @Test
    public void testRegFormNegative(){
        //WebDriverWait wait = new WebDriverWait(Browser.driver, 10);
        RegistrationPage.goTo();

        PageVerification.verifyPage("Register Account", "Not the page you expect!");

        RegistrationPage.clickContinue();

        RegistrationPage.negativeTestEmptyFields();
    }

    @Test
    public void testSuccessfulLogin() {
        //WebDriverWait wait = new WebDriverWait(Browser.driver, 10);
        AccountLoginPage.goTo();
        AccountLoginPage.enterLoginData("todorov@gmail.com", "proba123VV");
        PageVerification.verifyPage("My Account", "Login not successful");
    }

    @AfterMethod
    public void quit() {
        Browser.quit();
    }
}
