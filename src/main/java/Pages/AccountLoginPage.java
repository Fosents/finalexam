package Pages;

import org.openqa.selenium.By;
import utils.Browser;

public class AccountLoginPage {
    /**
     * Method to open the login page
     */
    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg/");
        Browser.driver.findElement(By.xpath("//div//span[contains(text(), 'My Account')]")).click();
        Browser.driver.findElement(By.xpath("//div//a[contains(text(), 'Login')]")).click();
    }

    /**
     * Method to enter login data
     * @param emailAddress - enter email address
     * @param password - enter the password
     */
    public static void enterLoginData(String emailAddress, String password) {

        Browser.driver.findElement(By.id("input-email")).sendKeys(emailAddress);
        Browser.driver.findElement(By.id("input-password")).sendKeys(password);
        Browser.driver.findElement(By.xpath("//input[@class='btn btn-primary']")).click();

    }
}
