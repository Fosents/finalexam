package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.Browser;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Registration Page of Pragmatic
 */
public class RegistrationPage {
    /**
     * Method to open the registration form
     */
    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg/");
        Browser.driver.findElement(By.xpath("//div//span[contains(text(), 'My Account')]")).click();
        Browser.driver.findElement(By.xpath("//div//a[contains(text(), 'Register')]")).click();
    }

    /**
     * Method to enter the registration form data.
     * @param firstName - String first name
     * @param lastName - String last name
     * @param eMail - String e-mail
     * @param telephone - String telephone
     * @param password - String password
     * @param passwordConfirm - String confirm password
     */
    public static void enterData(String firstName, String lastName, String eMail,
                                 String telephone, String password, String passwordConfirm) {

        Browser.driver.findElement(By.id("input-firstname")).sendKeys(firstName);
        Browser.driver.findElement(By.id("input-lastname")).sendKeys(lastName);
        Browser.driver.findElement(By.id("input-email")).sendKeys(eMail);
        Browser.driver.findElement(By.id("input-telephone")).sendKeys(telephone);
        Browser.driver.findElement(By.id("input-password")).sendKeys(password);
        Browser.driver.findElement(By.id("input-confirm")).sendKeys(passwordConfirm);

    }

    /**
     * Method to click Subscribe button
     */
    public static void clickSubscribe() {
        WebElement newsleter = Browser.driver.findElement(By.xpath("//div//label[@class='radio-inline']//input[@value=1]"));
        if (!newsleter.isSelected()) {
            newsleter.click();
        }
        assertTrue(newsleter.isSelected());
    }

    /**
     * Method to click agree
     */
    public static void clickAgree() {
        WebElement agree = Browser.driver.findElement(By.xpath("//div//input[@name='agree']"));
        if (!agree.isSelected()) {
            agree.click();
        }
        assertTrue(agree.isSelected());
    }

    /**
     * Method to click continue
     */
    public static void clickContinue() {
        Browser.driver.findElement(By.xpath("//div//input[@class='btn btn-primary']")).click();
    }

    /**
     * Method to test negative login
     */
    public static void negativeTestEmptyFields() {
        WebElement firstError = Browser.driver
                .findElement(By.xpath("//div[@class='alert alert-danger alert-dismissible']"));
        assertTrue(firstError.getText().contains("Warning: You must agree to the Privacy Policy!"));
        WebElement secondError = Browser.driver
                .findElement(By.xpath("//div//input[@id='input-firstname']/following-sibling::div"));
        assertTrue(secondError.getText().contains("First Name must be between 1 and 32 characters!"));
        WebElement thirdError = Browser.driver
                .findElement(By.xpath("//div//input[@id='input-lastname']/following-sibling::div"));
        assertTrue(thirdError.getText().contains("Last Name must be between 1 and 32 characters!"));
        WebElement forthError = Browser.driver
                .findElement(By.xpath("//div//input[@id='input-email']/following-sibling::div"));
        assertTrue(forthError.getText().contains("E-Mail Address does not appear to be valid!"));
        WebElement fifthError = Browser.driver
                .findElement(By.xpath("//div//input[@id='input-telephone']/following-sibling::div"));
        assertTrue(fifthError.getText().contains("Telephone must be between 3 and 32 characters!"));
        WebElement sixthError = Browser.driver
                .findElement(By.xpath("//div//input[@id='input-password']/following-sibling::div"));
        assertTrue(sixthError.getText().contains("Password must be between 4 and 20 characters!"));
    }
}
