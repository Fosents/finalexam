package utils;

import org.testng.Assert;

public class PageVerification {
    /**
     * method to verify if the page is as expected
     * @param expectedTitle - the title you expect
     * @param errorMessage - error message if not as expected
     */
    public static void verifyPage(String expectedTitle, String errorMessage) {
        String actualTitle = Browser.driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle, errorMessage);

    }
}
