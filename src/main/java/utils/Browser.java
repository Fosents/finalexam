package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Class to manage the browser.
 */
public class Browser {

    public static WebDriver driver;

    /**
     * Method to open a chrome browser
     */
    public static void open() {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    /**
     * Method to quit the browser
     */
    public static void quit() {
        driver.quit();
    }
}
